/*******************************************************************************
 * Copyright (c) 2015 Intecs.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 * Contributors:
 *     Nicholas Pacini
 *******************************************************************************/
package org.polarsys.chess.test.runtime.tests;

import org.eclipse.uml2.uml.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.polarsys.chess.core.profiles.CHESSProfileManager;
import org.polarsys.chess.test.runtime.util.TestUtil;

public class TestApplyCHESSStereotype {

	private Element model;

	@Before
	public void loadModel(){
		model = TestUtil.loadTestModel("CHESSTestModels", "TestApplyCHESSStereotypeModel.uml");
	}
	
	
	@Test @Ignore
	public void testApplyCHESSStereotype() {
		
		CHESSProfileManager.applyCHESSStereotype(model);
		
		Assert.assertTrue(model.getAppliedStereotype("CHESS::Core::CHESS") != null);
	}

}
