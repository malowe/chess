/*******************************************************************************
 *                  CHESS core plugin
 *
 *               Copyright (C) 2011-2015
 *            Mälardalen University, Sweden
 *
 *
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License
 *  v1.0 which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v20.html
 *******************************************************************************/
package org.polarsys.chess.fla.common;

import java.util.ArrayList;
import java.util.List;

public class FlaLogger implements org.polarsys.chess.fla.flamm.analysis.FlaLogger {

	List<String> errors = new ArrayList<String>();
	List<String> warnings = new ArrayList<String>();
	
	public void logError(String message) {
		errors.add(message);
	}
	
	public void logWarning(String message) {
		warnings.add(message);
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	public List<String> getWarnings() {
		return warnings;
	}
}
