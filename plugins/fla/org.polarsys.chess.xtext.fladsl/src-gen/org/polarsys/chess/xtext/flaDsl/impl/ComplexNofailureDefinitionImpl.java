/**
 */
package org.polarsys.chess.xtext.flaDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.polarsys.chess.xtext.flaDsl.ComplexNofailureDefinition;
import org.polarsys.chess.xtext.flaDsl.FlaDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Nofailure Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ComplexNofailureDefinitionImpl extends DefinitionsImpl implements ComplexNofailureDefinition
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComplexNofailureDefinitionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return FlaDslPackage.Literals.COMPLEX_NOFAILURE_DEFINITION;
  }

} //ComplexNofailureDefinitionImpl
