/**
 */
package org.polarsys.chess.xtext.flaDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Nofailure Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getComplexNofailureDefinition()
 * @model
 * @generated
 */
public interface ComplexNofailureDefinition extends Definitions
{
} // ComplexNofailureDefinition
