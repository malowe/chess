/**
 */
package org.polarsys.chess.xtext.flaDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ACI Davoidable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getA <em>A</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getC <em>C</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getI <em>I</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getD <em>D</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDavoidable()
 * @model
 * @generated
 */
public interface ACIDavoidable extends EObject
{
  /**
   * Returns the value of the '<em><b>A</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Aavoidable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>A</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>A</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Aavoidable
   * @see #setA(Aavoidable)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDavoidable_A()
   * @model
   * @generated
   */
  Aavoidable getA();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getA <em>A</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>A</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Aavoidable
   * @see #getA()
   * @generated
   */
  void setA(Aavoidable value);

  /**
   * Returns the value of the '<em><b>C</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Cavoidable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>C</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>C</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Cavoidable
   * @see #setC(Cavoidable)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDavoidable_C()
   * @model
   * @generated
   */
  Cavoidable getC();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getC <em>C</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>C</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Cavoidable
   * @see #getC()
   * @generated
   */
  void setC(Cavoidable value);

  /**
   * Returns the value of the '<em><b>I</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Iavoidable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>I</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>I</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Iavoidable
   * @see #setI(Iavoidable)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDavoidable_I()
   * @model
   * @generated
   */
  Iavoidable getI();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getI <em>I</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>I</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Iavoidable
   * @see #getI()
   * @generated
   */
  void setI(Iavoidable value);

  /**
   * Returns the value of the '<em><b>D</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Davoidable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>D</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>D</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Davoidable
   * @see #setD(Davoidable)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDavoidable_D()
   * @model
   * @generated
   */
  Davoidable getD();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDavoidable#getD <em>D</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>D</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Davoidable
   * @see #getD()
   * @generated
   */
  void setD(Davoidable value);

} // ACIDavoidable
