/**
 */
package org.polarsys.chess.xtext.flaDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definitions</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getDefinitions()
 * @model
 * @generated
 */
public interface Definitions extends EObject
{
} // Definitions
