/**
 */
package org.polarsys.chess.xtext.flaDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ACID Mitigation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getA <em>A</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getC <em>C</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getI <em>I</em>}</li>
 *   <li>{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getD <em>D</em>}</li>
 * </ul>
 *
 * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDMitigation()
 * @model
 * @generated
 */
public interface ACIDMitigation extends EObject
{
  /**
   * Returns the value of the '<em><b>A</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Amitigation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>A</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>A</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Amitigation
   * @see #setA(Amitigation)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDMitigation_A()
   * @model
   * @generated
   */
  Amitigation getA();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getA <em>A</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>A</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Amitigation
   * @see #getA()
   * @generated
   */
  void setA(Amitigation value);

  /**
   * Returns the value of the '<em><b>C</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Cmitigation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>C</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>C</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Cmitigation
   * @see #setC(Cmitigation)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDMitigation_C()
   * @model
   * @generated
   */
  Cmitigation getC();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getC <em>C</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>C</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Cmitigation
   * @see #getC()
   * @generated
   */
  void setC(Cmitigation value);

  /**
   * Returns the value of the '<em><b>I</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Imitigation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>I</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>I</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Imitigation
   * @see #setI(Imitigation)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDMitigation_I()
   * @model
   * @generated
   */
  Imitigation getI();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getI <em>I</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>I</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Imitigation
   * @see #getI()
   * @generated
   */
  void setI(Imitigation value);

  /**
   * Returns the value of the '<em><b>D</b></em>' attribute.
   * The literals are from the enumeration {@link org.polarsys.chess.xtext.flaDsl.Dmitigation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>D</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>D</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Dmitigation
   * @see #setD(Dmitigation)
   * @see org.polarsys.chess.xtext.flaDsl.FlaDslPackage#getACIDMitigation_D()
   * @model
   * @generated
   */
  Dmitigation getD();

  /**
   * Sets the value of the '{@link org.polarsys.chess.xtext.flaDsl.ACIDMitigation#getD <em>D</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>D</em>' attribute.
   * @see org.polarsys.chess.xtext.flaDsl.Dmitigation
   * @see #getD()
   * @generated
   */
  void setD(Dmitigation value);

} // ACIDMitigation
