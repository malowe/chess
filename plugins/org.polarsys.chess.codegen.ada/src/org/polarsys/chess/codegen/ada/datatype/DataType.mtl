[comment encoding = UTF-8 /]
[comment
-----------------------------------------------------------------------
--                Ada infrastructural code generator                 -- 
--                  for the CHESS component model                    --
--                                                                   --
--                    Copyright (C) 2011-2012                        --
--                 University of Padova, ITALY                       --
--                                                                   --
-- Author: Marco Panunzio         panunzio@math.unipd.it             --
--                                                                   --
-- All rights reserved. This program and the accompanying materials  --
-- are made available under the terms of the Eclipse Public License  --
-- v1.0 which accompanies this distribution, and is available at     --
-- http://www.eclipse.org/legal/epl-v20.html                         --
-----------------------------------------------------------------------
/]
[module DataType('http://www.eclipse.org/uml2/3.0.0/UML')]
[import org::polarsys::chess::codegen::ada::main::Copyright /]

[template public generateDataType(model: Model, procNode : InstanceSpecification) {
    procNodeName : String = procNode.name;
	primitiveTypeList : Sequence(PrimitiveType) = PrimitiveType.allInstances()->asSequence();
    enumerationList : Sequence(Enumeration) = Enumeration.allInstances()->asSequence();
	dataTypeList : Sequence(DataType) = DataType.allInstances()->select(dt | dt.oclIsTypeOf(DataType))->asSequence();
	BoundedSubtypeQN : String = 'MARTE::MARTE_Annexes::VSL::DataTypes::BoundedSubtype';
	CollectionTypeQN : String = 'MARTE::MARTE_Annexes::VSL::DataTypes::CollectionType';
	TupleTypeQN : String = 'MARTE::MARTE_Annexes::VSL::DataTypes::TupleType';
}
]

[file (procNodeName.concat('/src/datatype/datatype.ads').toLower(), false, 'UTF-8')]
[generateCopyrightAda(model)/]

package Datatype is

[for (pt : PrimitiveType | primitiveTypeList)]
	[if (pt.name = 'Integer')]
	Integer_Default_Value : constant Integer := 0;
	[elseif (pt.name = 'Float')]
	Float_Default_Value : constant Float := 0.0;
	[elseif (pt.name = 'Natural')]
	Natural_Default_Value : constant Natural := 0;
	[elseif (pt.name = 'Boolean')]
	Boolean_Default_Value : constant Boolean := False;
	[else]
	[comment Primitive type with constraints/]	
	[if (pt.ownedRule->select(c | c.name = 'digits')->asSequence()->size() = 1)]
		[let digits : Constraint = pt.ownedRule->select(c | c.name = 'digits')->filter(Constraint)->any(true)]
			[if (pt.ownedRule->select(c | c.name = 'range')->asSequence()->size() = 1)]
				[let range : Constraint = pt.ownedRule->select(c | c.name = 'range')->filter(Constraint)->any(true)]
	type [pt.name /] is digits [digits.specification->filter(LiteralInteger).value /] range [range.specification->filter(Interval).min->filter(LiteralString).value/] .. [range.specification->filter(Interval).max->filter(LiteralString).value/];
	[pt.name/]_Default_Value : constant [pt.name/] := 0.0;
				[/let]
			[else]
	type [pt.name /] is digits [digits.specification->filter(LiteralInteger).value /];
	[pt.name/]_Default_Value : constant [pt.name/] := 0.0;
			[/if]
		[/let]
		[else]
		[if (pt.ownedRule->select(c | c.name = 'range')->asSequence()->size() = 1)]
			[let range : Constraint = pt.ownedRule->select(c | c.name = 'range')->filter(Constraint)->any(true)]
	type [pt.name /] is new Float range [range.specification->filter(Interval).min->filter(LiteralString).value/] .. [range.specification->filter(Interval).max->filter(LiteralString).value/];
	[pt.name/]_Default_Value : constant [pt.name/] := 0.0;
			[/let]
		[/if]
	[/if]     
	[/if]
[/for]

[for (en : Enumeration | enumerationList)]
	type [en.name/] is ([en.ownedLiteral->first().name/][for (el : EnumerationLiteral | en.ownedLiteral->excluding(en.ownedLiteral->first()))], [el.name/][/for]);
	for [en.name/] use ([en.ownedLiteral->first().name/] => 0[for (el : EnumerationLiteral | en.ownedLiteral->excluding(en.ownedLiteral->first()))], [el.name/] => [i/][/for]);
	pragma Convention(C, [en.name/]);

	[en.name/]_Default_Value : constant [en.name/] := [en.ownedLiteral->first().name/];
[/for]

[for (dt : DataType | dataTypeList)]
	[if (dt.getAppliedStereotype(BoundedSubtypeQN)->notEmpty())]
	[comment MARTE BoundedSubtype: assume isMinOpen = isMaxOpen = false/]
		[let boundStereo : Stereotype = dt.getAppliedStereotype(BoundedSubtypeQN)]
	subtype [dt.name/] is [dt.getValue(boundStereo, 'baseType').oclAsType(DataType).name/] range [dt.getValue(boundStereo, 'minValue')/] .. [dt.getValue(boundStereo, 'maxValue')/];
		[/let]
	[/if]
	[if (dt.getAppliedStereotype(CollectionTypeQN)->notEmpty())]
	[comment MARTE CollectionType: assume range of array is known (i.e no *)/]
		[let collStereo : Stereotype = dt.getAppliedStereotype(CollectionTypeQN)]
		[let collAttrib : Property = dt.getValue(collStereo, 'collectionAttrib').oclAsType(Property)]
	type [dt.name/] is array ([collAttrib.lower/]..[collAttrib.upper/]) of [collAttrib.type.name/];
		[/let]
		[/let]
	[/if]
	[if (dt.getAppliedStereotype(TupleTypeQN)->notEmpty())]
		[let tupleStereo : Stereotype = dt.getAppliedStereotype(TupleTypeQN)]
		[let tupleAttrib : Sequence(Property) = getTupleAttributes(dt, tupleStereo)]
	type [dt.name/] is 
	   record
	   [for (prop : Property | tupleAttrib)]
	      [prop.name/] : [prop.type.name/];
	   [/for]
	   end record;
	   [/let]
	   [/let]
	[/if]

[/for]
end Datatype;
[/file]
[/template]

[query public getTupleAttributes(dt : DataType, stereo : Stereotype) : Sequence(Property) 
= invoke('org.polarsys.chess.codegen.ada.service.UML2Service', 'getTupleAttributes(org.eclipse.uml2.uml.DataType, org.eclipse.uml2.uml.Stereotype)', Sequence{dt, stereo})/]
