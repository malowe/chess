/*******************************************************************************
 * Copyright (C) 2020 Fondazione Bruno Kessler.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 ******************************************************************************/
package org.polarsys.chess.contracts.verificationService.test.runtime.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.rules.ErrorCollector;

import eu.fbk.eclipse.standardtools.utils.core.utils.StringArrayUtil;

public class TestResultsUtil {
	private static final String configFileName = "configTest.properties";

	public static boolean compareTwoFilesIgnoreEOL(Path p1, Path p2) throws IOException {
		BufferedReader reader1 = new BufferedReader(new FileReader(p1.toFile()));
		BufferedReader reader2 = new BufferedReader(new FileReader(p2.toFile()));
		String line1 = reader1.readLine();
		String line2 = reader2.readLine();
		boolean areEqual = true;

		while (line1 != null || line2 != null) {
			if (line1 == null || line2 == null) {
				areEqual = false;
				break;
			} else if (!StringArrayUtil.equalsIgnoreNewlineStyle(line1, line2)) {
				areEqual = false;
				break;
			}
			line1 = reader1.readLine();
			line2 = reader2.readLine();
		}
		reader1.close();
		reader2.close();

		return areEqual;
	}
	
	public static String cleanDirectory(String propertyDirectoryPathName) throws IOException {
		String workspaceDir = getProperty(propertyDirectoryPathName);
		File workspaceDirFile = new File(workspaceDir);
		FileUtils.deleteDirectory(workspaceDirFile);
		workspaceDirFile.mkdirs();
		return workspaceDir;
	}
	
	public static String getProperty(String key) throws IOException {
		File configFile = new File(configFileName);

		FileReader reader = new FileReader(configFile);
		Properties props = new Properties();
		props.load(reader);
		reader.close();
		return props.getProperty(key);

	}
	
	public static void dirsAreEqual(String path1, String path2,ErrorCollector collector) throws IOException{
		verifyDirsAreEqual(Paths.get(path1), Paths.get(path2),collector);
		verifyDirsAreEqual(Paths.get(path2), Paths.get(path1),collector);
	
	}
	
	public static void dirsHaveSameSize(String path1, String path2,ErrorCollector collector) throws IOException{
		verifyDirsHaveSameSize(Paths.get(path1), Paths.get(path2),collector);
		verifyDirsHaveSameSize(Paths.get(path2), Paths.get(path1),collector);
	
	}
	
	public static void dirsHaveSameFilesNames(String path1, String path2,ErrorCollector collector) throws IOException{
		verifyDirsHaveSameFilesNames(Paths.get(path1), Paths.get(path2),collector);
		verifyDirsHaveSameFilesNames(Paths.get(path2), Paths.get(path1),collector);
	
	}
	
	public static void verifyDirsAreEqual(Path correctResultsDir, Path toCheckResultsDir,ErrorCollector collector) throws IOException {

		Files.walkFileTree(correctResultsDir, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				FileVisitResult result = super.visitFile(file, attrs);

				// get the relative file name from path "one"
				Path correctFilePath = correctResultsDir.relativize(file);
				// construct the path for the counterpart file in "other"
				Path toCheckFilePath = toCheckResultsDir.resolve(correctFilePath);

				try {
					Assert.assertTrue(file + " is not equal to " + toCheckFilePath,
							TestResultsUtil.compareTwoFilesIgnoreEOL(file, toCheckFilePath));
				} catch (Throwable t) {
					collector.addError(t);
				}

				return result;
			}
		});
	}

	public static void verifyDirsHaveSameFilesNames(Path correctResultsDir, Path toCheckResultsDir, ErrorCollector collector) throws IOException {
		Files.walkFileTree(correctResultsDir, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				FileVisitResult result = super.visitFile(file, attrs);

				// get the relative file name from path "one"
				Path correctFilePath = correctResultsDir.relativize(file);
				// construct the path for the counterpart file in "other"
				Path toCheckFilePath = toCheckResultsDir.resolve(correctFilePath);

				try {
					Assert.assertTrue(file + " has not the same size of " + toCheckFilePath,
							sameName(file, toCheckFilePath));
				} catch (Throwable t) {
					collector.addError(t);
				}

				return result;
			}
		});
	}
	
	public static void verifyDirsHaveSameSize(Path correctResultsDir, Path toCheckResultsDir, ErrorCollector collector) throws IOException {
		Files.walkFileTree(correctResultsDir, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				FileVisitResult result = super.visitFile(file, attrs);

				// get the relative file name from path "one"
				Path correctFilePath = correctResultsDir.relativize(file);
				// construct the path for the counterpart file in "other"
				Path toCheckFilePath = toCheckResultsDir.resolve(correctFilePath);

				try {
					Assert.assertTrue(file + " has not the same size of " + toCheckFilePath,
							sameSize(file, toCheckFilePath));
				} catch (Throwable t) {
					collector.addError(t);
				}

				return result;
			}
		});
	}

	private static String getFileSizeKiloBytes(File file) {
		return (double) file.length() / 1024 + "  kb";
	}

	private static boolean sameSize(Path p1, Path p2) {

		String sizeFile1 = getFileSizeKiloBytes(p1.toFile());
		String sizeFile2 = getFileSizeKiloBytes(p2.toFile());

		return sizeFile1.equals(sizeFile2);
	}
	
	private static boolean sameName(Path p1, Path p2) {

		String fileName1 = p1.toFile().getName();
		String fileName2 = p2.toFile().getName();

		return fileName1.equals(fileName2);
	}
}
