/**
 */
package org.polarsys.chess.mobius.model.SAN.impl;

import org.eclipse.emf.ecore.EClass;
import org.polarsys.chess.mobius.model.SAN.InstantaneousActivity;
import org.polarsys.chess.mobius.model.SAN.SANModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instantaneous Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InstantaneousActivityImpl extends ActivityImpl implements InstantaneousActivity {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstantaneousActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SANModelPackage.Literals.INSTANTANEOUS_ACTIVITY;
	}

} //InstantaneousActivityImpl
